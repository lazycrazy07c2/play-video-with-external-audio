#!/bin/sh
#
# modify font size by .ass subs
# param1: sub file in
# param2: sub file out
# param3: [-d to debug]
#

SUB_IN="${1}"
SUB_OUT="${2}"
DEBUG=${3}

if [ "${DEBUG}" = "-d" ] ; then
	echo "\n"
	echo "sub file in: ${SUB_IN}"
	echo "sub file out: ${SUB_OUT}"
	echo "SUBS_SIZE: ${SUBS_SIZE}"
	echo "SUBS_MIN_SIZE: ${SUBS_MIN_SIZE}"
fi

if [ -z "${SUB_IN}" ] || [ ! -f "${SUB_IN}" ] || [ -z "${SUB_OUT}" ] || [ ! -f "${SUB_OUT}" ] ; then echo "use ${0} sub_file_in sub_file_out" ; exit 1; fi


# modify font size
if [ ! -z "${SUBS_SIZE}" ]; then
    awk -v SUBS_SIZE="${SUBS_SIZE}" -v SUBS_MIN_SIZE="${SUBS_MIN_SIZE}" '
        {
            if ( match($0, "^(Style: [^,]*,[^,]*,)([^,]*)(,.*)$") > 0 ) {
                # if it is a style line, change the size, ...
                
                delimiter=","
                # split by delimiter
                n=split($0, arr, delimiter)
                
                # write the size to array if size has be to change
                if ( SUBS_SIZE > 0 ) {
                    # change to SUBS_SIZE
                    if ( arr[3] > SUBS_SIZE ) { arr[3] = SUBS_SIZE }
                } else if ( arr[3] > SUBS_MIN_SIZE ) {
                    # change the size
                    HALF_SUBS_SIZE = SUBS_SIZE
                    
                    while ( arr[3] + HALF_SUBS_SIZE < SUBS_MIN_SIZE ) {
                        # half of the HALF_SUBS_SIZE
                        if ( HALF_SUBS_SIZE % 2 != 0 ) {
                            HALF_SUBS_SIZE = ( HALF_SUBS_SIZE + 1 ) / 2
                        } else {
                            HALF_SUBS_SIZE = HALF_SUBS_SIZE / 2
                        }
                    }
                    
                    if ( arr[3] + HALF_SUBS_SIZE >= SUBS_MIN_SIZE ) {
                        arr[3] = arr[3] + HALF_SUBS_SIZE
                    }
                }
                
                # print the array back with the same delimiter
                ret=""
                for (i = 1; i <= n; i++) { ret = ret delimiter arr[i] }
                print substr(ret, 2)
            } else if ( match($0, "{[^}]*\\\\fs[0-9]+.*}") > 0 ) {
                # if it is a line with font size, change the size, ...
                
                delimiter="\\\\fs"
                # split by delimiter
                n=split($0, arr, delimiter)
                
                for (i = 2; i <= n; i++) {
                    old_size = arr[i]
                    sub("[^0-9].*$", "", old_size)
                    size = old_size
                    if ( SUBS_SIZE > 0 ) {
                        # change to SUBS_SIZE
                        if ( size > SUBS_SIZE ) { size = SUBS_SIZE }
                    } else if ( size > SUBS_MIN_SIZE ) {
                        # change the size
                        HALF_SUBS_SIZE = SUBS_SIZE
                        
                        while ( size + HALF_SUBS_SIZE < SUBS_MIN_SIZE ) {
                            # half of the HALF_SUBS_SIZE
                            if ( HALF_SUBS_SIZE % 2 != 0 ) {
                                HALF_SUBS_SIZE = ( HALF_SUBS_SIZE + 1 ) / 2
                            } else {
                                HALF_SUBS_SIZE = HALF_SUBS_SIZE / 2
                            }
                        }
                        
                        if ( size + HALF_SUBS_SIZE >= SUBS_MIN_SIZE ) {
                            size = size + HALF_SUBS_SIZE
                        }
                    }
                    # write the size to array if size has be to change
                    sub(old_size, size, arr[i])
                }
                
                # print the array back with the same delimiter
                delimiter = substr(delimiter, 2)
                ret=""
                for (i = 1; i <= n; i++) { ret = ret delimiter arr[i] }
                print substr(ret, 4)
            } else {
                # ... else print line without changes
                
                print
            }
        }
    ' "${SUB_IN}" > "${SUB_OUT}"
    
    if [ "${DEBUG}" = "-d" ] ; then
        echo "diff \"${SUB_IN}\" \"${SUB_OUT}\":"
        diff "${SUB_IN}" "${SUB_OUT}"
        echo ""
    fi
fi

# end
