#!/bin/sh
#
# start video with vlc
# param1: video dir
# param2: [sound dir]
# param3: [add param]
# param4: [start with]
# param5: [-d to debug]
#
ulimit -v 4096000

export SUBS_DIR='subs' # directory will be created and used for subs
# changes subs size
export SUBS_SIZE='-15'
export SUBS_MIN_SIZE='16'

export USE_FFMPEG='true' # merges video and audio files to one before play
export ADD_PARAMS='-f --file-caching 3000'
#export ADD_PARAMS='--audio-track 1 -f --file-caching 3000'

bash play_with_vlc.sh "${@}"
