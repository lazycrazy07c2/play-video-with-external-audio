#!/bin/bash
#
# start video with vlc
# param1: video dir
# param2: [sound dir]
# param3: [add param]
# param4: [start with]
# param5: [-d to debug]
#

UNWANTED='/\.unwanted/ \.png'
VIDEO_SUFIXS='\.mkv \.avi \.mp4'
SOUND_SUFIXS='\.mka \.mp3 \.ac3'
SUBS_SUFIXS='\.ass \.srt'

VIDEO_DIR=${1}
SOUND_DIR=${2}
ADD_PARAM=${3}
MIN=${4}
DEBUG=${5}

if [ "${DEBUG}" = "-d" ] ; then
	echo -e "\n"
	echo "video dir: ${VIDEO_DIR}"
	echo "sound dir: ${SOUND_DIR}"
	echo "add param: ${ADD_PARAM}"
	echo "SUBS_DIR: ${SUBS_DIR}"
	echo "SUBS_SIZE: ${SUBS_SIZE}"
	echo "SUBS_MIN_SIZE: ${SUBS_MIN_SIZE}"
fi

if [ -z "${VIDEO_DIR}" ] || [ ! -d "${VIDEO_DIR}" ] ; then echo "use ${0} video_dir [sound_dir] [add_param] [start_with]" ; exit ; fi
if [ -z "${MIN}" ] ; then
	MIN=1
fi

ADD_PARAMS=${ADD_PARAM}' '${ADD_PARAMS}
# use array
ADD_PARAM_ARRAY=(${ADD_PARAMS})


# remove unwanted and ${@:2} from ${1} and return it
remove_unwanted() {
	RET="${1}" ; shift
#	if [ "${DEBUG}" = "-d" ] ; then
#		echo "funktion remove_unwanted(): ${@}" >&2
#	fi
	for j in ${UNWANTED} ${@} ; do
		RET=`echo "${RET}" | grep -v ${j}`
	done
	echo "${RET}"
}

cd "${VIDEO_DIR}"
SEARCH_PATTERN="^${MIN}[^0-9]\|[^0-9]${MIN}[^0-9]"

# search for first file
VIDEO_FILE=`ls --file-type | grep "${SEARCH_PATTERN}"`
VIDEO_FILE=`remove_unwanted "${VIDEO_FILE}" ${SOUND_SUFIXS} ${SUBS_SUFIXS}`
VIDEO_FILE=`echo "${VIDEO_FILE}" | grep -m1 "${SEARCH_PATTERN}"`

# get file list
for j in ${VIDEO_SUFIXS} ; do
	FILE_LIST="${FILE_LIST}\n`ls | grep "${j}$"`"
done
FILE_LIST=`echo -e "${FILE_LIST}" | sed '/^$/d'`
FILE_LIST=`remove_unwanted "${FILE_LIST}"`

if [ ! -f "${VIDEO_FILE}" ] ; then
	if [ "${DEBUG}" = "-d" ] ; then
		echo "first video file not found: ${VIDEO_FILE}, try the search with find"
	fi
	# try the search with find
	VIDEO_FILE=`find . -maxdepth 1 -type f`
	VIDEO_FILE=`remove_unwanted "${VIDEO_FILE}" ${SOUND_SUFIXS} ${SUBS_SUFIXS}`
	if [ -f "${VIDEO_FILE}" ] ; then
		if [ "${DEBUG}" = "-d" ] ; then
			echo "first video file found: ${VIDEO_FILE}"
		fi
		# find a number in file
		MIN=`echo "${VIDEO_FILE}" | grep -o '[0-9]*'`
		for i in ${MIN} ; do
			MIN=${i}
		done
		SEARCH_PATTERN="^${MIN}[^0-9]\|[^0-9]${MIN}[^0-9]"
	else
# 		if [ "${DEBUG}" = "-d" ] ; then
# 			echo "first video file not found: ${VIDEO_FILE}"
# 		fi
		# search without maxdepth
		VIDEO_FILE=`find . -type f -iname "*${MIN}*" | grep "${SEARCH_PATTERN}"`
		VIDEO_FILE=`remove_unwanted "${VIDEO_FILE}" ${SOUND_SUFIXS} ${SUBS_SUFIXS}`
		if [ -f "${VIDEO_FILE}" ] ; then
			# get file list
			FILE_LIST=""
			for j in ${VIDEO_SUFIXS} ; do
				FILE_LIST="${FILE_LIST}\n`find . -type f | grep "${j}$"`"
			done
			FILE_LIST=`echo "${FILE_LIST}" | sed '/^$/d'`
			FILE_LIST=`remove_unwanted "${FILE_LIST}"`
		fi
	fi
fi
if [ "${DEBUG}" = "-d" ] ; then
	echo "first video file: ${VIDEO_FILE}"
fi
if [ ! -f "${VIDEO_FILE}" ] ; then echo "video file ${VIDEO_FILE} not exists" ; exit ; fi

FILE_SUFIX=`echo "${VIDEO_FILE}" | grep -o '\.[^\.]*$'`
if [ "${DEBUG}" = "-d" ] ; then
	echo "file sufix: \"${FILE_SUFIX}\""
fi

FILE_LIST=`echo "${FILE_LIST}" | grep "${FILE_SUFIX}$"`
if [ "${DEBUG}" = "-d" ] ; then
	echo -e "file list:\n${FILE_LIST}"
fi


# search for subs
SUB_DIRS=`ls | grep -i "sub"`
if [ -z "${SUB_DIRS}" ] && [ ! -z "${SOUND_DIR}" ]; then
	# try the search by sound dir
	SUB_DIRS=`ls "${SOUND_DIR}" | grep -i "sub"`
fi
if [ -z "${SUB_DIRS}" ] ; then
	# try the search by sufixs
	for k in ${SUBS_SUFIXS} ; do
		# try the search by sound dir too
		for j in "." "${SOUND_DIR}" ; do
			if [ -z "${j}" ] ; then
				continue
			fi
			SUB=`find "${j}" -iname "*${k}" | grep "${SEARCH_PATTERN}"`
			SUB=`remove_unwanted "${SUB}" ${SOUND_SUFIXS} ${VIDEO_SUFIXS} "^\./${SUBS_DIR}/"`
			if [ -f "${SUB}" ] ; then
				break
			fi
			if [ ! -z "${SUB}" ] ; then
				SUB=`echo "${SUB}" | head -n 1`
				if [ "${DEBUG}" = "-d" ] ; then
					echo "sub file: \"${SUB}\", k: \"${k}\", j: \"${j}\""
				fi
				break
			fi
			# nothing found
			if [ "${DEBUG}" = "-d" ] ; then
				echo "sub file: \"${SUB}\" not exists, k: \"${k}\", j: \"${j}\""
			fi
		done
		if [ -f "${SUB}" ] ; then
			SUB_DIRS=`dirname "${SUB}"`
			break
		fi
	done
	if [ "${DEBUG}" = "-d" ] ; then
		echo "sub dir: \"${SUB_DIRS}\""
	fi
fi
if [ ! -z "${SUB_DIRS}" ] ; then
# 	if [ "${DEBUG}" = "-d" ] ; then
# 		echo -e "sub dirs:\n${SUB_DIRS}"
# 	fi
	if [ ! -d "${SUBS_DIR}" ] ; then
		mkdir "${SUBS_DIR}"
	fi
fi


MIN_NUM=${MIN//[^0-9]}
if [ -z "${MIN_NUM}" ] ; then
	ORIG_MIN=${MIN}
	MIN_NUM=1
	MAX=1
else
	MAX=`expr ${MIN_NUM} + 20`
	AFTER_PART=${MIN#*${MIN_NUM}}
	BEFORE_PART=${MIN%${MIN_NUM}*}
fi
if [ ${MIN_NUM:0:1} -eq 0 ] ; then
	SEQ=`seq -w ${MIN_NUM} ${MAX}`
else
	SEQ=`seq ${MIN_NUM} ${MAX}`
fi
if [ "${DEBUG}" = "-d" ] ; then
	echo "min: \"${MIN_NUM}\", max: \"${MAX}\", seq: `echo "${SEQ}" | sed -z 's/\n/ /g'`"
fi


# disable autostart
#vlc --no-playlist-autostart --one-instance &
#sleep 2

for i in ${SEQ} ; do
	if [ "${DEBUG}" = "-d" ] ; then
		echo ""
	fi
	if [ ! -z "${ORIG_MIN}" ] ; then
		i=${ORIG_MIN}
	else
		i=${BEFORE_PART}${i}${AFTER_PART}
	fi
	if [ "${DEBUG}" = "-d" ] ; then
		echo "i: \"${i}\""
	fi
	
	SEARCH_PATTERN="^${i}[^0-9]\|[^0-9]${i}[^0-9]"
	
	
	# play file
	PLAY=`echo "${FILE_LIST}" | grep "${SEARCH_PATTERN}"`
	if [ ! -f "${PLAY}" ] ; then
		if [ "${DEBUG}" = "-d" ] ; then
			echo "play file: ${PLAY}"
		fi
		FILE_LIST="`ls *${FILE_SUFIX}`"
		PLAY=`echo "${FILE_LIST}" | grep "${SEARCH_PATTERN}"`
	fi
	if [ "${DEBUG}" = "-d" ] ; then
		echo "play file: ${PLAY}"
	fi
	if [ ! -f "${PLAY}" ] ; then echo "video file: ${PLAY} not exists, i: ${i}" ; exit ; fi
	
	
	# copy the sub file to "${SUBS_DIR}"
	if [ ! -z "${SUB_DIRS}" ] ; then
		for k in ${SUBS_SUFIXS} ; do
			# try the search by sound dir too
			for j in "." "${SOUND_DIR}" ; do
				SUB=`find "${j}" -iname "*${k}" | grep "${SEARCH_PATTERN}"`
				SUB=`remove_unwanted "${SUB}" ${SOUND_SUFIXS} ${VIDEO_SUFIXS} "^\./${SUBS_DIR}/"`
				if [ -f "${SUB}" ] ; then
					break
				fi
				if [ `echo "${SUB}" | grep -c "${SEARCH_PATTERN}"` -gt 0 ] ; then
					echo -e "more sub files found:\n${SUB}"
					read -p "choose one of them: " S_NO # read into ${S_NO}
					SUB=`echo "${SUB}" | tail -n "+${S_NO}" | head -n 1`
					break
				fi
				# nothing found
				if [ "${DEBUG}" = "-d" ] ; then
					echo "sub file: ${SUB} not exists, k: ${k}, j: ${j}"
				fi
			done
			if [ -f "${SUB}" ] ; then
				break
			fi
		done
		if [ ! -z "${SUB}" ] ; then
			echo "copy sub file \"${SUB}\" to \"${SUBS_DIR}\""
			cp "${SUB}" "${SUBS_DIR}"
			if [ ! -z "${SUBS_SIZE}" ] ; then
				SUB_FILE="${SUBS_DIR}/`basename "${SUB}"`"
				
				sh `dirname "${0}"`/modify_subs.sh "${SUB}" "${SUB_FILE}" "${DEBUG}"
			fi
		fi
	fi
	
	
	if [ ! -z "${SOUND_DIR}" ] ; then
		# sound file
		# search for sound sufixs
		for k in ${SOUND_SUFIXS} ; do
			S=`find "${SOUND_DIR}" -iname "*${k}" | grep "${SEARCH_PATTERN}"`
			S=`remove_unwanted "${S}" ${SUBS_SUFIXS} ${VIDEO_SUFIXS} ${FILE_SUFIX}`
			if [ ! -f "${S}" ] ; then
				if [ `echo "${S}" | grep -c "${SEARCH_PATTERN}"` -gt 0 ] ; then
					echo -e "more sound files found:\n${S}"
					read -p "choose one of them: " S_NO # read into ${S_NO}
					S=`echo "${S}" | tail -n "+${S_NO}" | head -n 1`
					break
				fi
				if [ "${DEBUG}" = "-d" ] ; then
					echo "sound file: ${S} not exists, k: ${k}"
				fi
			else
				break
			fi
		done
		if [ ! -f "${S}" ] && [ -z "${NO_SOUND}" ] ; then
			# ask only once
			echo "sound file: ${S} not exists, i: ${i}, continue? (y/n)"
			read NO_SOUND # read into ${NO_SOUND}
			if [ "${NO_SOUND}" != "y" ] ; then exit ; fi
		fi
		if [ "${DEBUG}" = "-d" ] ; then
			echo "sound file: ${S}"
		fi
		if [ -f "${S}" ] ; then
			if [ "${USE_FFMPEG}" = 'true' ] ; then
				# merge files and play
				PLAY_TMP="play_tmp_${PLAY}"
				if [ "${DEBUG}" = "-d" ] ; then
					echo "faking ffmpeg ..."
				else
					echo "ffmpeg to ${PLAY_TMP}"
					nice ffmpeg -n -i "${PLAY}" -i "${S}" -map 0 -map -0:a -map 1:a -c copy "${PLAY_TMP}" >/dev/null 2>&1
				fi
				if [ $? -gt 0 ] && [ -f "${PLAY_TMP}" ] ; then
					rm "${PLAY_TMP}"
					exit 1
				fi
				PLAY="${PLAY_TMP}"
				if [ -f "${SUB_FILE}" ]; then
					mv "${SUB_FILE}" "${SUBS_DIR}/play_tmp_`basename "${SUB_FILE}"`"
				fi
			else
				# add the sound file
				ADD_PARAM_ARRAY=(--input-slave "${S}" ${ADD_PARAMS})
			fi
		fi
		
		# play
		if [ "${DEBUG}" = "-d" ] ; then
			echo "faking vlc ..."
			sleep 4
			ERR=${?}
		else
			echo -e "\n\nvlc \"${PLAY}\"; add param: ${ADD_PARAM_ARRAY[@]} --play-and-exit"
			
			nice vlc "${PLAY}" "${ADD_PARAM_ARRAY[@]}" --play-and-exit >/dev/null 2>&1
			ERR=${?}
		fi
		if [ -f "${PLAY_TMP}" ] ; then
			rm "${PLAY_TMP}"
		fi
		if [ "${ERR}" -gt 0 ] ; then
			exit 1
		fi
	else
		IS_STARTED=`ps -ef | grep -c [v]lc`
		
		echo -e "\n\nvlc \"${PLAY}\"; add param: ${ADD_PARAM_ARRAY[@]} --playlist-enqueue --one-instance"
		
		# add to playlist
		nice vlc "${PLAY}" "${ADD_PARAM_ARRAY[@]}" --playlist-enqueue --one-instance >/dev/null 2>&1 &
		
		if [ "${DEBUG}" = "-d" ] ; then
			echo "is_started: ${IS_STARTED}"
		fi
		if [ ${IS_STARTED} -eq 0 ] ; then
			while [ -z "${IS_PLAY}" ] ; do
				if [ "${DEBUG}" = "-d" ] ; then
					echo "sleep"
				fi
				sleep 2
				IS_PLAY=`ps -ef | grep [v]lc`
			done
			sleep 10
		fi
		sleep 60
	fi
done

if [ ! -z "${SUBS}" ] ; then
	while [ ! -z "${IS_PLAY}" ] ; do
		if [ "${DEBUG}" = "-d" ] ; then
			echo "sleep"
		fi
		sleep 4
		IS_PLAY=`ps -ef | grep [v]lc`
	done
	rm -rf "${SUBS_DIR}"
fi

if [ "${DEBUG}" = "-d" ] ; then
	echo "end"
fi
# end
