## Usage

to start the video with **01**

`sh play.sh /myVideoDir/ . "" "01"`

to start the video with **_01_**

`sh play.sh /myVideoDir/ . "" "_01_"`

to start the video in fullscreen

`sh play.sh /myVideoDir/ . "-f" "01"`


to start the video with first audio-track

`sh play.sh /myVideoDir/ . "--audio-track 1 -f" "01"`


to start the video where sound file is outside the video directory, relative to the video directory

`sh play.sh /myVideoDir/ .. "" "01"`
